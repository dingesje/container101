var express = require('express');
var app = express();

// prometheus
const promBundle = require("express-prom-bundle");
const { Histogram } = require('prom-client');

var Prometheus = require('./modules/counter');

// Start using Prometheus
app.use(Prometheus.requestCounters);  

// Add the options to the prometheus middleware most option are for http_request_duration_seconds histogram metric
const metricsMiddleware = promBundle({
    includeMethod: true,
    includePath: true,
    includeStatusCode: true,
    includeUp: true,
    customLabels: {project_name: 'dockerapp', project_type: 'test_metrics_labels'},
    metricsType: Histogram,
    promClient: {
        collectDefaultMetrics: {
        }
      }
});

// add the prometheus middleware to all routes
app.use(metricsMiddleware)

// the app itself
app.use(express.static(__dirname + '/www'));
app.listen(8080, function () {      
    console.log('Listening at http://localhost:8080');      
  });