const colors = ['#FF5733', '#FFFF66', '#66FF66', '#66FFFF', '#FF66FF', '#9966FF', '#CC6699'];

function getRandomColor() {
    const randomIndex = Math.floor(Math.random() * colors.length);
    return colors[randomIndex];
  }

const colorBox = document.getElementById('colour-box');
colorBox.style.backgroundColor = getRandomColor();
